package com.ray.magicBlock;

/**
 * @author bo shen
 * @Description: 策略接口
 * @Class: Strategy
 * @Package com.ray.magicBlock
 * @date 2019/10/9 19:05
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface Strategy<P,O,R> {
    /**策略执行**/
    R execute(P p,O o);
}
