package com.ray.magicBlock.anno;

import java.lang.annotation.*;

/**
 * @author bo shen
 * @Description: 模块注释 把一个类注释成一个内部专有的Bean
 * @Class: Block
 * @Package com.ray.magicBlock
 * @date 2019/10/9 18:51
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Block {
    /**策略**/
    String strategy();
    /**分组**/
    String group();
    /**描述**/
    String desc() default "";
}
