package com.ray.magicBlock;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 配置
 * @Class: BlockConfig
 * @Package com.ray.magicBlock
 * @date 2020/2/15 19:55
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BlockConfig {
    /**路径***/
    private String packages;
}
