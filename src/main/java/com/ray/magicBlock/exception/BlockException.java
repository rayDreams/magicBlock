package com.ray.magicBlock.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: 异常信息
 * @Class: BlockException
 * @Package com.ray.magicBlock.exception
 * @date 2019/10/10 14:54
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BlockException extends RuntimeException{

    public static String Code_500= "500";
    private String code;

    /**
     * 构造一个业务异常类
     *
     * @param code 错误码,注意约定的业务错误码都是大于800的
     * @param message 错误信息
     */
    public BlockException(String code, String message){
        super(message);
        log.info(message);
        this.code = code;
    }

    /**
     * 返回错误码
     * @return 错误码
     */
    public String getCode() {
        return code;
    }
}
