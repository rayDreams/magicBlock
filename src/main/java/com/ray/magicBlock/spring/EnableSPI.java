package com.ray.magicBlock.spring;

import com.ray.magicBlock.BlockDispatch;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author bo shen
 * @Description: 开启导入导出拦截器
 * @Class: EnableImportExportAspect
 * @Package com.tf56.spring
 * @date 2019/11/21 9:19
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({BlockBeanFactory.class, BlockDispatch.class})
public @interface EnableSPI {
}
