package com.ray.magicBlock;

/**
 * @author bo shen
 * @Description: 工具
 * @Class: Utils
 * @Package com.ray.magicBlock
 * @date 2019/10/10 14:51
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class Utils {

    public static String getBeanName(String group,String value){
        return String.format("%s:%s",group,value);
    }
}
